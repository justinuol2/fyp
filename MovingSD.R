i<-1
MovingSD <- NULL

for (i in 1:10){
fileNames <- paste("DATA/PART1/",i,".txt",sep='')
series <- read.table(fileNames,header=TRUE)
close <- series[,"Close"]

sd <- 0
msd <- sd
n <- 2
for (n in 2:length(close)){
sd <- sd(close[1:n])
msd <- c(msd,sd)
n <- n+1
}
dates <- as.Date(series[,"Index"])
msd <- xts(msd,dates)

MovingSD <- cbind(MovingSD,close,msd)
i <- i+1
}
plot.zoo(MovingSD,col=c("red","blue"),main="Moving StandardDeviation")