#params   	<- list(lookback=50,series=1:10)

getNewPosList <- function(store, newRowList, params){
  
  if (is.null(store)) 
    store 	<- initStore(newRowList,params$series)
  else 
    store	<- updateStore(store, newRowList, params$series)
  
  pos <- rep(0,length(newRowList))		
  
  if (store$iter > params$lookback) {
    
    for (i in 2:length(params$series)) {
      
      
      #calculate the slow(lookback day) and fast(5day) EMA
         
      vl <- newRowList[[params$series[i]]]$Volume
      
        if (vl[[i]]>vl[[i-1]]||vl[[i]]==vl[[i-1]]){
          pos[params$series[i]] <- 1
        }				
        if(vl[[i]]<vl[[i-1]]){	
          pos[params$series[i]] <- -1
        }
    }
    #cat(store$poslist,'\n')
    
    
  }
  #cat(store$iter,"--pos---",pos,"\n")
  store$poslist   <- pos
  return(list(store=store,pos=pos))
}

initClStore  <- function(newRowList,series) {
  return(lapply(series, function(x) newRowList[[x]]$Close))
}

initVlstore  <- function(newRowList,series) {
  return(lapply(series, function(x) newRowList[[x]]$Volume))
}


updateClStore <- function(oldClStore, newRowList, series) {
  return(mapply(function(x,y) rbind(oldClStore[[x]],newRowList[[y]]$Close),
                1:length(series),series,SIMPLIFY=FALSE))
}

updateVlStore <- function(oldVlStore, newRowList, series) {
  return(mapply(function(x,y) rbind(oldVlStore[[x]],newRowList[[y]]$Volume),
                1:length(series),series,SIMPLIFY=FALSE))
}



initStore <- function(newRowList,series) {
  
  return(list(iter=1,
              cl=initClStore(newRowList,series),
              vl=initVlstore(newRowList,series),
              poslist <- rep(0,length(newRowList))
              		
  ))
}

updateStore <- function(store, newRowList, series) {
  store$iter 	<- store$iter + 1
  store$cl	<- updateClStore(store$cl,newRowList,series) 
  store$vl  <- updateVlStore(store$vl,newRowList,series)
  
  return(store)
  
}
