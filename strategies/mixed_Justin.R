#params <- list(series=c(1:10),lookback=,pairsTrade,pairs,nFast,nSlow,spBench,threshold)
##############################################################################
#################### FYP mixed trading  strategy  ############################
##############################################################################

getNewPosList <- function(store, newRowList, params) {
  
  if (is.null(store)){
    store 	<- initStore(newRowList,params$series)
  }else{ 
    store	<- updateStore(store, newRowList, params$series)
  }
  
  pos <- rep(0,length(newRowList))
  if (store$iter > params$lookback) {
    for (i in 1:length(params$series)) {
      stockForPairs <- FALSE
      if(params$pairsTrade){
        for(j in 1:length(params$pairs)){
          if(i == params$pairs[[j]][1] || i == params$pairs[[j]][2]){
            stockForPairs<- TRUE
          }
        }
      }
      
      if(!stockForPairs ){
        OHLC <- cbind(store$op[[i]],store$hi[[i]],store$lo[[i]],store$cl[[i]])
        adx <- ADX(OHLC,14)
        atr <- ATR(OHLC)
        
        if(!store$inTrade[params$series[i]]){ 
          if(last(adx$ADX)>20){
            fema <- EMA(store$cl[[i]], n=params$nFast)
            sema <- EMA(store$cl[[i]], n=params$nSlow)
            diffe <- fema - sema
            difDI <- adx$DIp - adx$DIn
            
            if(last(difDI)>0 && as.numeric(diffe[store$iter-1])<= 0 && as.numeric(diffe[store$iter])>=0){
              #cat('up\n')      
              pos[params$series[i]] <- round(last(1/atr$atr))*round(last(1/atr$atr))
              cat('in-up')
              store$inTrade[params$series[i]] <- TRUE
            }
            if(last(difDI)<0 && as.numeric(diffe[store$iter-1])>=0 && as.numeric(diffe[store$iter])<=0){
              #cat('in-down\n')
              pos[params$series[i]] <- -round(last(1/atr$atr))*round(last(1/atr$atr))
              store$inTrade[params$series[i]] <- TRUE
            }
          }
          if(last(adx$ADX)<20){ 
            pos[params$series[i]] <- posFunc(store,i,pos,params)*round(last(1/atr$atr))*round(last(1/atr$atr))
            if(pos[params$series[i]]!=0){
              #         cat('off\n')
              store$inTrade[params$series[i]] <- TRUE
            }
          }
        }
        if(store$posList[params$series[i]]!=0){
          pos[params$series[i]] <- round(last(1/atr$atr))*round(last(1/atr$atr))*store$posList[params$series[i]]/abs(store$posList[params$series[i]])
          if(pos[params$series[i]]<1){pos[params$series[i]]<-1}
          #stoploss
          sar <- SAR(cbind(store$hi[[i]],store$lo[[i]]))
          if(as.numeric(sar[store$iter-1]-store$cl[[i]][store$iter - 1])*(as.numeric(last(sar)-last(store$cl[[i]]))) < 0){
            pos[params$series[i]] <- 0
            store$inTrade[params$series[i]] <- FALSE
          }          
        } 
      }
      for(j in 1:length(params$pairs)){
        if(i==params$pairs[[j]][1] && params$pairsTrade ){
          
          if(store$iter > 60){
            k <- params$pairs[[j]][1]
            q <- params$pairs[[j]][2]
            positionRatio <- lag(SMA(store$cl[[k]]/store$cl[[q]]),n=50)
            spread <- store$cl[[k]]- positionRatio * store$cl[[q]]
            sp <- last(spread)
            #cat(k,'-',q,'\n')
            #cat(sp,'\n')
            #bbands <-  BBands(spread,sd=1.4,n=20)
            #dn     <- last(bbands$dn)  
            #up   	<- last(bbands$up)
            
            if(store$inTrade[q]){
              pos[k] <- store$posList[k]
              pos[q] <- store$posList[q]
              
              if(abs(sp) <= params$spBench[j]){
                pos[k] <- 0
                pos[q] <- 0
                store$inTrade[k] <-FALSE
                store$inTrade[q] <-FALSE
                
              }
            }
            
            if(!store$inTrade[k]){
              
              if(sp < -params$spBench[j]){
                pos[k] <- -1
                pos[q] <- round(last(positionRatio))
              }
              
              if(sp > params$spBench[j]){
                pos[k] <- 1
                pos[q] <- -round(last(positionRatio))
              }
              
              if(pos[k] != 0){
                store$inTrade[k] <-TRUE
                store$inTrade[q] <-TRUE
                
              }
            }	
          }
        }
      }
    }
  }
  
  cat(store$iter,"-------", pos,"---","\n")
  store$posList <- pos
  return(list(store=store,pos=pos))
}

initClStore  <- function(newRowList,series) {
  return(lapply(series, function(x) newRowList[[x]]$Close))
}
initHiStore  <- function(newRowList,series) {
  return(lapply(series, function(x) newRowList[[x]]$High))
}
initLoStore  <- function(newRowList,series) {
  return(lapply(series, function(x) newRowList[[x]]$Low))
}
initOpStore  <- function(newRowList,series) {
  return(lapply(series, function(x) newRowList[[x]]$Open))
}

updateClStore <- function(oldClStore, newRowList, series) {
  return(mapply(function(x,y) rbind(oldClStore[[x]],newRowList[[y]]$Close),
                1:length(series),series, SIMPLIFY=FALSE))
}
updateHiStore <- function(oldHiStore, newRowList, series) {
  return(mapply(function(x,y) rbind(oldHiStore[[x]],newRowList[[y]]$High),
                1:length(series),series, SIMPLIFY=FALSE))
}
updateLoStore <- function(oldLoStore, newRowList, series) {
  return(mapply(function(x,y) rbind(oldLoStore[[x]],newRowList[[y]]$Low),
                1:length(series),series, SIMPLIFY=FALSE))
}
updateOpStore <- function(oldOpStore, newRowList, series) {
  return(mapply(function(x,y) rbind(oldOpStore[[x]],newRowList[[y]]$Open),
                1:length(series),series, SIMPLIFY=FALSE))
}

initStore <- function(newRowList,series) {	
  return(list(iter=1,
              pairsInTrade = FALSE,
              cl=initClStore(newRowList,series),
              hi=initHiStore(newRowList,series),
              lo=initLoStore(newRowList,series),
              op=initOpStore(newRowList,series),
              posList<-rep(0,length(newRowList)),
              inTrade = rep(FALSE,length(newRowList))))
}

updateStore <- function(store, newRowList, series) {
  store$iter 	<- store$iter + 1
  store$op	<- updateOpStore(store$op,newRowList,series) 
  store$hi	<- updateHiStore(store$hi,newRowList,series) 
  store$lo	<- updateLoStore(store$lo,newRowList,series)	
  store$cl	<- updateClStore(store$cl,newRowList,series) 
  return(store)
}

posFunc <- function(store,i,pos,params){
  if(last(RSI(store$cl[[i]],n=params$lookback)) > params$threshold){pos[params$series[i]] <- 3}
  if(last(RSI(store$cl[[i]],n=params$lookback)) < 100 -params$threshold){pos[params$series[i]] <- -3}
  if(last(RSI(store$cl[[i]],n=params$lookback)) > params$threshold+10){pos[params$series[i]] <- 2}
  if(last(RSI(store$cl[[i]],n=params$lookback)) < 90 - params$threshold){pos[params$series[i]] <- -2}
  
  return(pos[params$series[i]])
}