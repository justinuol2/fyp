####################################################################################
# STRATEGY CODE
#
# getNewPosList is the interface between the backtester and your strategies
####################################################################################

# It's arguments and return list should stay the same.

####################################################################################
# ARGUMENTS 
####################################################################################

# store: see explanation under returned list below
# newRowList: this is a list of single-row OHLCV xts objects, one for each series
# params: these are the strategy params

####################################################################################
# RETURNED LIST
####################################################################################

# This function must return a list with the two named memebers, store and pos

# store contains data required by the strategy is in the variable store
# store is returned by the function and passed back to it the next time 
# it is called by the backetester.

# pos is a vector containing the number of lots for each series that the
# strategy will hold on the current day
#
# The i'th entry of the integer vector pos (of length nseries) represents 
# the number of contract you want to be long (short) on the next trading day if 
# the entry is positive (negative).

# Entries of pos should be  integers 
####################################################################################

getNewPosList <- function(store, newRowList, params) {

	if (is.null(store)) 
		store 	<- initStore(newRowList)
	else 
		store	<- updateStore(store, newRowList)
	
	if (store$iter <= params$lookback)
		pos <- rep(0,length(newRowList))
	else
		pos <- sapply(longOrShort(store$cl,store$iter),function(x) if (x) 1 else -1)

	return(list(store=store,pos=pos))
}

####################################################################################
# All the subsequent functions were designed to simplify and 
# improve the readaility of getNewPos(); 
#
# While not required, this type of function-based approach is advisable 
# and these functions will likely have analogues in your strategies
####################################################################################

initClStore  <- function(newRowList) {
	clStore <- matrix(0,nrow=2000,ncol=length(newRowList))
	clStore <- updateClStore(clStore, newRowList, iter=1)
    return(clStore)
}

updateClStore <- function(clStore, newRowList, iter) {
    for (i in 1:length(newRowList))
        clStore[iter,i] <- as.numeric(newRowList[[i]]$Close)
    return(clStore)
}

initStore <- function(newRowList) {
	return(list(iter=1,cl=initClStore(newRowList)))
}

updateStore <- function(store, newRowList) {
	store$iter 	<- store$iter + 1
	store$cl	<- updateClStore(store$cl,newRowList,store$iter) 
	return(store)
}

longOrShort <-	function(clStore,iter) {
	startIndex <- iter - params$lookback - 1
	last(RSI(clStore[startIndex:iter,],n=params$lookback)) > params$threshold
}
